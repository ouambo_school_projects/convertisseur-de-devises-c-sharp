﻿
using System.Drawing;
using System.IO;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Text;

namespace Convertisseur_De_Monnaie_Groupe10
{
    internal class Program
    {
        //Chemins des fichiers configuration et sauvegarde
        public static string cheminConfig = "configuration.txt";
        public static string cheminSauvegarde = "sauvegarde.txt";

        //variable globale contenant le montant saisi par l'utilisateur
        public static double montantCourant = -1;

        //variable globale contenant la devise choisi par l'utilisateur
        public static int deviseCourant = 0;

        //variable globale contenant toutes les lignes du fichier configuration
        public static string[] listeDevises;

        //varibale globale contenant le nombre de devises du fichier configuration
        public static int nombreDevises;

        //Les couleurs utilisées pour l'affichage
        public static ConsoleColor darkmagnata = ConsoleColor.DarkMagenta;
        public static ConsoleColor darkcyan = ConsoleColor.DarkCyan;
        public static ConsoleColor cyan = ConsoleColor.Cyan;
        public static ConsoleColor menu = ConsoleColor.Gray;
        public static ConsoleColor red = ConsoleColor.Red;
        public static ConsoleColor green = ConsoleColor.DarkGreen;


        static void Main(string[] args)
        {
            bool repeter = true;

            //La boucle suivante permet à l'utilisateur d'effectuer plussieurs conversions
            do
            {
                Console.Clear();
                EffectuerConversion();

                Console.WriteLine("\n\n");
                tabulation(4);
                Console.Write("Voulez vous effectuer une autre conversion ? (O/N) : ");

                //Si l'utilisateur saisi Oui, une nouvelle conversion est engagée
                repeter = (Console.ReadLine().ToUpper().Equals("O")) ? true : false;

            } while (repeter);

            Console.WriteLine();
            tabulation(4);
            Console.Write("Merci et à la prochaine ... Appuyez sur une touche du clavier pour quitter  ");
            Console.ReadKey();
        }


        public static void EffectuerConversion()
        {
            /*
             * Argument : Pas d'argument
             * Type de retour : void
             * Description : elle sera appelée tant que l’utilisateur souhaite faire une
             *               conversion. Elle fera appel à presque toutes les autres fonctions.
             */

            //Appel successif des fonctions necessaires pour effectuer une conversion
            AfficherMenu();
            LireDevise(ref deviseCourant);
            LireMontant();

            double[] resultat = ConvertirMontant(montantCourant);

            //Si la sauvegarde réussie, on affcihe le resultat. Sinon, un message d'erreur est affiché
            if (SauvegarderResultat(resultat))
                AfficherResultat(resultat);
            else
            {
                tabulation(4);
                Console.WriteLine("Une erreur s'est produite lors de la sauvegarde du resutat!");
            }
                
        }


        public static void tabulation(int nbTab) {
            /**
             * Minie fonction permettant de deplacer le curseur à une position donnée 
             * */
            Console.Write(new string(' ', nbTab));
        }


        public static void FormaterIndiceDevise(int indice, string devise)
        {
            /**
             * Minie fonction permettant d'afficher les devises disponibles
             * ainsi que leurs indices, avec les couleurs.
             * */
            Console.Write("[");
            Console.ForegroundColor= darkmagnata;
            Console.Write(indice);
            Console.ResetColor();
            Console.Write("]");
            Console.ForegroundColor = darkcyan;
            Console.Write(devise);
            Console.ResetColor();
        }

        public static void FormaterMontantResultat(double montant, string devise)
        {
            /**
             * Minie fonction permettant d'afficher les autres devises
             * ainsi que les montants issues de la conversion, avec les couleurs.
             * */
            Console.ForegroundColor = green;
            Console.Write(montant.ToString("0.00"));
            Console.ResetColor();
            Console.ForegroundColor = darkcyan;
            Console.Write(" " +devise+ "   ");
            Console.ResetColor();
        }

        public static string ExtraitreNomDevise(string devise)
        {
            /**
             * Minie fonction permettant d'extraire le nom de la devise d'une ligne (String)
             * */
            string[] chaine = devise.Split('|');
            return chaine[1];
        }

        public static void AfficherMenu()
        {
            /*
             * Argument : Pas d'argument
             * Type de retour : void
             * Description : elle affiche un message de bienvenue, elle affiche aussi les 
             *               devises enregistrées dans le fichier Configuration.txt. Elle 
             *               fera appel à la fonction ExtraireDeviseEnregistrer()
             */

            //Personnalisation du titre de la console
            Console.Title = "@ Groupe 10 Programmation C# - Hiver 2023 - Collège la cité";

            // extraction de toutes les devises de configuration.txt
            // et initialiation de la variable globale listeDevises
            listeDevises = ExtraireDevise(cheminConfig);  
            
            //initialisation de la variable globales nombreDevises
            nombreDevises= listeDevises.Length - 1;             

            string bienvenue = @" 
        _ _ _ _ _ _ _                                              _ _ _ _ _ _                                       
       /  _ _ _ _ _ _|           Groupe 10 - C# - @2023           /  _ _ _ _  \
       | |                                                        | |        \ \
       | |                     Veuillez Choisir une devise        | |_ _ _ _ / /
       | |                    puis, saisissez votre montant.      |  _ _ _ _  |
       | |                                                        | |        \ \
       | |                                                        | |         \ \         
       | \_ _ _ _ _ _ _                                           | \_ _ _ _ _/ / 
        \_ _ _ _ _ _ _ |onvertisseur de Monnaie                   \_ _ _ _ _ __/ ienvenue ... !!!                          ";

            Console.ForegroundColor = menu;
            Console.WriteLine(bienvenue);
            Console.ResetColor();
            

            int positionCurseur = 2;

            for(int i=1; i<=nombreDevises; i++)
            {
                if ((i-1) % 5 == 0)
                {
                    Console.WriteLine("\n");
                    positionCurseur = 2;
                }

                tabulation(positionCurseur);

                //Affichage d'une devise a sa position souhaite
                FormaterIndiceDevise(i, ExtraitreNomDevise(listeDevises[i]));

                positionCurseur++;
            }
            Console.WriteLine();
        }


        public static string[] ExtraireDevise(string chemin)
        {
            /*
             * Argument : Chemin du fichier Configuration.txt (string) 
             * Type de retour : un tableau de chaine de caractères (string), chaque chaine
             *                  correspond à une ligne du fichier « configuration.txt ».
             * Description : elle ouvre le fichier configuration.txt et retourne tous ses lignes,
             *               chaque ligne correspond à une devise ainsi que ses formules de convention
             *               (taux de conversion)
             */

            return File.ReadAllLines(chemin);
        }


        public static string ExtraireLigne(string[] listedevises, int indiceLigneDevise)
        {
            /*
            * Argument : tableau de chaine (ensemble des lignes du fichier configuration.txt) et 
            *            un entier qui correspond a l'indice de la ligne recherchee
            * Type de retour : Une chaine de caractere qui correspond a la ligne recherche
            * Description : elle parcourt la liste des lignes et retourne une ligne precise.
            */

            int i = 1;
            while (true)
            {       //Si l'indice correspond, on retourne la ligne et la fonction prend fin.
                if (Int32.Parse(listedevises[i].Split('|')[0].Trim()) == indiceLigneDevise)
                  return listedevises[i];
                else
                   i++;
            }
        }



        public static double LireMontant()
        {
            /*
             * Argument         : Pas d'argument
             * Type de retour   : double (c’est le montant saisir au clavier par l’utilisateur)
             * Description      : elle demande à l’utilisateur d’entrer un montant valide et répète
             *                    la demande tant que le montant saisi n’est pas valide.
             */
            do
            {
                tabulation(4);
                Console.Write("Veuillez saisir votre montant : ");
                Console.ForegroundColor = green;

                if (double.TryParse(Console.ReadLine(), out montantCourant))
                {
                    tabulation(4);
                    Console.ForegroundColor = red;
                    Console.WriteLine((montantCourant < 0) ? "Montant non valide. Le montant doit etre positif \n" : "\n");
                    Console.ResetColor();
                }
                else
                {
                    tabulation(4);
                    Console.ForegroundColor = red;
                    Console.WriteLine("Type de donnee non valide! Montant doit etre un nombre réel \n");
                    Console.ResetColor();
                }
            } while (montantCourant <= 0);

            return montantCourant;
        }


        public static void LireDevise(ref int devisecourant)
        {
            /*
             * Argument : deviseCourant (par reference, ce qui va permettre à la fonction de modifier sa valeur)
             * Type de retour : pas de valeur de retour
             * Description : elle demande à l’utilisateur de choisir le nombre qui correspond 
             *               à sa devise et répète la demande tant que le nombre saisi n’est pas valide 
             */
            Console.WriteLine();
            do
            {
                tabulation(4);
                Console.Write("Veuillez saisir le nombre correspondant à votre devise : ");
                if (Int32.TryParse(Console.ReadLine(), out devisecourant))
                {   
                    tabulation(4);
                    Console.ForegroundColor = red;
                    Console.WriteLine((devisecourant < 1 || devisecourant > nombreDevises) ? "Devise non valide \n" : "\n");
                    Console.ResetColor();
                }
                else
                {
                    tabulation(4);
                    Console.ForegroundColor = red;
                    Console.WriteLine("Type de donnee non valide! Devise est un entier compris entre 1 et {0} \n", nombreDevises);
                    Console.ResetColor();
                }

            } while (devisecourant < 1 || devisecourant > nombreDevises);
        }


        public static double[] ExtraireTaux(string ligne)
        {
            /*
             * Argument : string (correspond à une ligne du fichier configuuration.txt)
             * Type de retour : un tableau de double 
             * Description : elle va extraire juste les taux de la ligne prise en paramètres. 
             */

            //Decoupage de la chaine avec "|" comme delimiteur
            string[] tableauChaine = ligne.Split('|');
        
            double[] tableauTaux = new double[nombreDevises];

            //Suppression des vides et conversion du reste de la chaine en nombre reel
            for(int i=2; i<nombreDevises+2; i++)
                tableauTaux[i-2] = double.Parse(tableauChaine[i].Trim());

            return tableauTaux;
        }


        public static double[] ConvertirMontant(double montant)
        {
            /*
             * Argument : double
             * Type de retour : tableau de double (c’est un tableau qui contient les nouveaux 
             *                  montant dans toutes les devises sauvegardées)
             * Description : elle converti le montant saisi au clavier par l’utilisateur vers 
             *               toutes les autres devises sauvegardées. Elle fait appel à la 
             *               fonction ExtraireTaux() 
             */
            //Extraction des taux de la devise courante vers les autres devises
            double[] tabTaux = ExtraireTaux(ExtraireLigne(listeDevises, deviseCourant));

            double[] tabMontants = new double[nombreDevises];

            //Calcul des taux vers toutes les autres devises
            for (int i = 0; i < nombreDevises; i++)
                tabMontants[i] = montant * tabTaux[i];

            return tabMontants;
        }



        public static bool SauvegarderResultat(double[] resultat)
        {
            /*
             * Argument : tableau de double 
             * Type de retour : bool (pour vérifier si la sauvegarde a réussi)
             * Description : elle enregistre le résultat dans un fichier sauvegarde.txt
             */
            string nomDeviseEntree = ExtraitreNomDevise(ExtraireLigne(listeDevises, deviseCourant)).Trim();

            string sauvegarde = new string("");

            sauvegarde = sauvegarde + montantCourant + " " + nomDeviseEntree + " = | ";

            //fabrication de la chaine de caractere contenant tout le resultat
            for(int i=0; i<nombreDevises; i++)
            {
                if(!nomDeviseEntree.Equals(ExtraitreNomDevise(listeDevises[i+1]).Trim()))
                    sauvegarde = sauvegarde + resultat[i].ToString("0.00") + " " + ExtraitreNomDevise(listeDevises[i+1]).Trim() + " | ";
            }

            //Tentatative de sauvegarde
            try
            {
                //Ouverture du fichier sauvegarde en mode modification sans ecraser l'ancien contenu
                StreamWriter stream = new StreamWriter(cheminSauvegarde, true, Encoding.ASCII);

                //Recuperation de la date et heure de l'enregistrement et ecriture dans le fichier
                stream.WriteLine(DateTime.Now.ToString("[dd/MM/yyyy HH:mm:ss") + "] ==> " + sauvegarde);
                stream.WriteLine();
                stream.Close();
                return true;
            }
            catch(Exception ex)
            {
                //Si la sauvegarde echoue, on retourne false
                return false;
            }
            
        }


        public static void AfficherResultat(double[] resultat)
        {
            /*
             * Argument : tableau de double 
             * Type de retour : void
             * Description : elle affiche le résultat sur la console. 
             */

            string nomDeviseEntree = ExtraitreNomDevise(ExtraireLigne(listeDevises, deviseCourant));
            nomDeviseEntree=nomDeviseEntree.Trim();

            tabulation(20);
            Console.ForegroundColor = cyan;
            Console.WriteLine("Resultat sauvegardé avec succes dans le fichier sauvegarde.txt\n");
            Console.ResetColor();
           

            tabulation(3);

            Console.ForegroundColor = green;
            Console.Write(montantCourant);
            Console.ResetColor();
            Console.Write($" {nomDeviseEntree} : ");

            int positionCurseur = 19;

            tabulation(positionCurseur);
            int nbAfficher = 0; ;

            for (int i = 1; i <= nombreDevises; i++)
            {
                //Cette condition permet d'aller a la ligne apres avoir affiche trois devises et leurs montants
                if (nbAfficher % 3 == 0 && !nomDeviseEntree.Equals(ExtraitreNomDevise(listeDevises[i]).Trim()))
                {
                    Console.WriteLine("\n");
                    positionCurseur = 19;
                    tabulation(positionCurseur);
                    Console.Write("= ");
                }

                //On affiche les devises qui sont differentes de la devise choisie par l'utilisateur
                if (!nomDeviseEntree.Equals(ExtraitreNomDevise(listeDevises[i]).Trim()))
                {
                    FormaterMontantResultat(resultat[i-1], ExtraitreNomDevise(listeDevises[i]).Trim());
                    nbAfficher++;
                }  
            }
        }
    }
}